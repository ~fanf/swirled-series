#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <err.h>

typedef unsigned uint;
typedef unsigned char byte;

#define PI 3.141592653589793238

// animation steps
#define A 45

// anti-alias oversampling
#define Q 4

// size of a tile
#define T (64*Q)
// number of squares
#define S 8

// pixel grid - we don't want the animation to be confined by the
// checker squares so we define this as a flat square rather than
// pix[S][S][T][T].
typedef bool pixmap[S*T][S*T];

// chirality of each truchet tile
typedef bool trucmap[S][S];

static void
tile_bulge(pixmap pix, uint sx, uint sy, bool chiral, bool invert, double tt) {
	uint t = tt * (T/2) * 15/16 + T/32;
	uint dlo = T/2 - t; dlo *= dlo;
	uint dhi = T/2 + t; dhi *= dhi;
	double alo = (1.0 - tt) * 1.1 * PI/4;
	double ahi = PI/2 - alo;
	for(uint tx0 = 0; tx0 < T; tx0++) {
		for(uint ty0 = 0; ty0 < T; ty0++) {
			uint tx1 = T - tx0 - 1;
			uint ty1 = T - ty0 - 1;
			uint d0 = tx0*tx0 + ty0*ty0;
			uint d1 = tx1*tx1 + ty1*ty1;
			double a0 = atan2((double)ty0, (double)tx0);
			double a1 = atan2((double)ty1, (double)tx1);
			bool c0 = dlo <= d0 && d0 < dhi &&
				  (a0 < alo || ahi < a0);
			bool c1 = dlo <= d1 && d1 < dhi &&
				  (a1 < alo || ahi < a1);
			uint tx = chiral ? tx1 : tx0;
			pix[sx*T+tx][sy*T+ty0] = (c0 | c1) ^ invert;
		}
	}
}

static void
tile_thick(pixmap pix, uint sx, uint sy, bool chiral, bool invert, double tt) {
	uint t = tt * (T/2) * 15/16 + T/32;
	uint lo = T/2 - t; lo *= lo;
	uint hi = T/2 + t; hi *= hi;
	for(uint tx0 = 0; tx0 < T; tx0++) {
		for(uint ty0 = 0; ty0 < T; ty0++) {
			uint tx1 = T - tx0 - 1;
			uint ty1 = T - ty0 - 1;
			uint d0 = tx0*tx0 + ty0*ty0;
			uint d1 = tx1*tx1 + ty1*ty1;
			bool c0 = lo <= d0 && d0 < hi;
			bool c1 = lo <= d1 && d1 < hi;
			uint tx = chiral ? tx1 : tx0;
			pix[sx*T+tx][sy*T+ty0] = (c0 | c1) ^ invert;
		}
	}
}

static void
truchet(pixmap pix, trucmap truc, bool i, bool j, double tt) {
	for(uint sx = 0; sx < S; sx++)
		for(uint sy = 0; sy < S; sy++)
			if(((sx^sy) & 1) == i)
				tile_bulge(pix, sx, sy, truc[sx][sy], j, tt);
			else
				tile_thick(pix, sx, sy, truc[sx][sy], j, tt);
}

static void
checker(pixmap pix, bool i) {
       for(uint sx = 0; sx < S; sx++)
               for(uint sy = 0; sy < S; sy++)
                       for(uint tx = 0; tx < T; tx++)
                               for(uint ty = 0; ty < T; ty++)
				       assert(pix[sx*T+tx][sy*T+ty] ==
					      (((sx^sy) & 1) == i));
}

static void
random_truchet(trucmap truc) {
	FILE *fp = fopen("/dev/urandom", "r");
	if(fp == NULL)
		err(1, "open /dev/urandom");
	byte buf[S*S/8];
	if(fread(buf, sizeof(*buf), sizeof(buf), fp) < sizeof(buf))
		err(1, "read /dev/urandom");
	fclose(fp);
	for(uint sx = 0; sx < S; sx++) {
		for(uint sy = 0; sy < S; sy++) {
			uint tile = S*sx+sy;
			uint byte = tile / 8;
			uint bit = tile % 8;
			truc[sx][sy] = (buf[byte] >> bit) & 1;
		}
	}
}

static void
pix2pgm_fp(FILE *fp, pixmap pix) {
	uint qq = Q*Q;
	fprintf(fp, "P2 %d %d %d\n", S*T/Q, S*T/Q, qq);
	uint c = 0;
	for(uint qx = 0; qx < S*T/Q; qx++) {
		for(uint qy = 0; qy < S*T/Q; qy++) {
			uint q = qq;
			for(uint px = 0; px < Q; px++)
				for(uint py = 0; py < Q; py++)
					q -= pix[Q*qx+px][Q*qy+py];
			fprintf(fp, "%u%c", q, ++c % 20 ? ' ' : '\n');
		}
	}
}

static void
pix2pgm_fn(const char *fn, pixmap pix) {
	FILE *fp = fopen(fn, "w");
	if(fp == NULL)
		err(1, "open %s", fn);
	pix2pgm_fp(fp, pix);
	if(ferror(fp) || fflush(fp) || fclose(fp))
		err(1, "write %s", fn);
}

static void
pix2pgm_count(pixmap pix, uint n) {
	char fn[64];
	snprintf(fn, sizeof(fn), "truchet-%03u.pgm", n);
	pix2pgm_fn(fn, pix);
	if(n % 16) return;
	printf("%s\r", fn);
	fflush(stdout);
}

static double
smoothly(uint i, uint n) {
	assert(i < n);
	double d = (double)i / (double)(n-1);
	return((sin(d * PI - PI/2) + 1) / 2);
}

int
main(void) {
	pixmap pix;
	trucmap truc;
	random_truchet(truc);
	for(uint i = 0; i < A; i++) {
		double t = smoothly(i, A);
		truchet(pix, truc, false, false, 1 - t);
		if(i == 0) checker(pix, true);
		pix2pgm_count(pix, 0*A + i);
		truchet(pix, truc, true, false, t);
		if(i == A-1) checker(pix, false);
		pix2pgm_count(pix, 1*A + i);
	}
	random_truchet(truc);
	for(uint i = 0; i < A; i++) {
		double t = smoothly(i, A);
		truchet(pix, truc, false, true, 1 - t);
		if(i == 0) checker(pix, false);
		pix2pgm_count(pix, 2*A + i);
		truchet(pix, truc, true, true, t);
		if(i == A-1) checker(pix, true);
		pix2pgm_count(pix, 3*A + i);
	}
	printf("\n");
}
