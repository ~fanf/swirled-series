#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <err.h>

#define PI 3.141592653589793238

#define S 512

#define BLUR 1.0

typedef unsigned uint;
typedef unsigned char byte;
typedef double pixmap[S][S];

static double
hilbert_xy(uint ax, uint ay, uint r) {
	uint x, y;
	switch(r) {
	case(0): x =       ax; y =       ay; break;
	case(1): x = S-1 - ay; y =       ax; break;
	case(2): x = S-1 - ax; y = S-1 - ay; break;
	case(3): x =       ay; y = S-1 - ax; break;
	default: assert(!"bug");
	}
	uint i = 0;
	for(uint n = S/2; n > 0; n /= 2) {
		uint rx = (x & n) > 0;
		uint ry = (y & n) > 0;
		i += n * n * ((3 * rx) ^ ry);
		if(ry == 0) {
			uint ox = x, oy = y;
			y = rx ? S-1 - ox : ox;
			x = rx ? S-1 - oy : oy;
		}
	}
	return((double)i / (S*S - 1.0));
}

static void
hilbert_fade(pixmap dst, pixmap old, pixmap new, uint r, double d) {
	assert(0.0 <= d && d <= 1.0);
	double hi = d * (1.0 + BLUR);
	double lo = hi - BLUR;
	for(uint x = 0; x < S; x++) {
		for(uint y = 0; y < S; y++) {
			double g = hilbert_xy(x, y, r);
			g	= g <= lo ? 0.0
				: g >= hi ? 1.0
				: (g - lo) / (hi - lo);
			dst[x][y]
				= old[x][y] * (0.0 + g)
				+ new[x][y] * (1.0 - g);
		}
	}
}

static void
pixmap_checker(pixmap pix, bool inv) {
	for(uint x = 0; x < S; x++) {
		for(uint y = 0; y < S; y++) {
			uint tx = x * 8 / S;
			uint ty = y * 8 / S;
			pix[x][y] = (tx^ty^inv) & 1;
		}
	}
}

static void
pixmap_clear(pixmap pix, bool inv) {
	for(uint x = 0; x < S; x++)
		for(uint y = 0; y < S; y++)
			pix[x][y] = inv;
}

static uint
atoui(const char *s) {
	char *e = NULL;
	unsigned long n = strtoul(s, &e, 10);
	if(n >= UINT_MAX || *e != '\0')
		errx(1, "invalid number %s", s);
	return((uint)n);
}

static void
pix2pgm_fp(FILE *fp, pixmap pix) {
	fprintf(fp, "P2 %d %d %d\n", S, S, 255);
	uint c = 0;
	for(uint x = 0; x < S; x++) {
		for(uint y = 0; y < S; y++) {
			double g = pix[x][y];
			assert(0.0 <= g && g <= 1.0);
			fprintf(fp, "%u%c",
				255 - (uint)round(g * 255),
				++c % 16 ? ' ' : '\n');
		}
	}
}

static void
pix2pgm_fn(const char *fn, pixmap pix) {
	FILE *fp = fopen(fn, "w");
	if(fp == NULL)
		err(1, "open %s", fn);
	pix2pgm_fp(fp, pix);
	if(ferror(fp) || fflush(fp) || fclose(fp))
		err(1, "write %s", fn);
}

static void
pix2pgm_count(pixmap pix, uint i) {
	char fn[64];
	snprintf(fn, sizeof(fn), "hilbert-%03u.pgm", i);
	pix2pgm_fn(fn, pix);
	if(i % 16) return;
	printf("%s\r", fn);
	fflush(stdout);
}

int
main(int argc, char *argv[]) {
	if(argc != 3)
		errx(1, "usage: %s count index", argv[0]);
	uint n = atoui(argv[1]);
	uint i = atoui(argv[2]);
	if(i >= n)
		errx(1, "%d must be less than %d", i, n);
	if(n % 4)
		errx(1, "%d must be a multiple of 4", n);
	uint m = n / 4;
	uint r = i / m;
	double d = (double)(i % m) / (double)(m-1);
	pixmap pix, old, new;
	switch(r) {
	case(0):
		pixmap_checker(old, 0); pixmap_clear(new, 1);
		break;
	case(2):
		d += 1;
		// FALLTHRU
	case(1):
		pixmap_clear(old, 1); pixmap_clear(new, 0);
		d /= 2;
		break;
	case(3):
		pixmap_clear(old, 0); pixmap_checker(new, 0); break;
	default:
		assert(!"bug");
	}
	hilbert_fade(pix, old, new, 3, d);
	pix2pgm_count(pix, i);
}
